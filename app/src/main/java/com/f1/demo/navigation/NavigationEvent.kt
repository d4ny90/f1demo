package com.f1.demo.navigation

import androidx.navigation.NavDirections

data class NavigationEvent(val navDirections: NavDirections)