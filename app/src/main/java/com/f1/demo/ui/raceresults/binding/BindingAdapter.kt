package com.f1.demo.ui.raceresults.binding

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.*
import com.f1.demo.base.adapter.BindingListAdapter
import com.f1.demo.ui.raceresults.viewmodel.DriverResultViewModel
import com.f1.demo.ui.raceresults.viewmodel.RaceTableViewModel


@BindingAdapter(value = ["raceResults"])
fun bindRaceResults(recyclerView: RecyclerView, raceResults: ArrayList<RaceTableViewModel>) {
    var adapter: BindingListAdapter<RaceTableViewModel>? =
        recyclerView.adapter as? BindingListAdapter<RaceTableViewModel>

    if (adapter == null) {
        recyclerView.layoutManager = LinearLayoutManager(recyclerView.context, LinearLayoutManager.HORIZONTAL, false)
        PagerSnapHelper().attachToRecyclerView(recyclerView)

        adapter = BindingListAdapter()

        adapter.addTypeAdapter(RaceTableAdapter())

        recyclerView.adapter = adapter

    }

    adapter.list = ArrayList(raceResults)
    adapter.notifyDataSetChanged()
}

@BindingAdapter(value = ["driverResults"])
fun bindDriverResults(recyclerView: RecyclerView, driverResults: ArrayList<DriverResultViewModel>) {
    var adapter: BindingListAdapter<DriverResultViewModel>? =
        recyclerView.adapter as? BindingListAdapter<DriverResultViewModel>

    if (adapter == null) {
        recyclerView.layoutManager = LinearLayoutManager(recyclerView.context, LinearLayoutManager.VERTICAL, false)

        adapter = BindingListAdapter()

        adapter.addTypeAdapter(DriverResultAdapter())

        recyclerView.adapter = adapter

    }

    val diffResult = DiffUtil.calculateDiff(DriverDiffUtilCallback(adapter.list, driverResults))

    adapter.list.clear()
    adapter.list.addAll(driverResults)

    diffResult.dispatchUpdatesTo(adapter)
}
