package com.f1.demo.ui.main

import com.f1.demo.ui.main.viewmodel.MainViewModel
import com.f1.demo.data.sources.ergast.api.ErgastApi
import com.f1.demo.di.ViewModelProviderFactory
import dagger.Module
import dagger.Provides
import io.reactivex.Single

@Module
class MainActivityModule {

    @Provides
    internal fun provideViewModel(): ViewModelProviderFactory<MainViewModel> {

        return object : ViewModelProviderFactory<MainViewModel>() {
            override fun create(): MainViewModel {
                return MainViewModel()
            }
        }
    }

}