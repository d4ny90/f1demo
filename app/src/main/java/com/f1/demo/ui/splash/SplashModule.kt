package com.f1.demo.ui.splash

import com.f1.demo.di.ViewModelProviderFactory
import com.f1.demo.ui.splash.viewmodel.SplashViewModel
import dagger.Module
import dagger.Provides

@Module
class SplashModule {

    @Provides
    internal fun provideViewModel(): ViewModelProviderFactory<SplashViewModel> {

        return object : ViewModelProviderFactory<SplashViewModel>() {
            override fun create(): SplashViewModel {
                return SplashViewModel()
            }
        }
    }
}