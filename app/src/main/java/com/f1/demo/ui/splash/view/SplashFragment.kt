package com.f1.demo.ui.splash.view

import com.f1.demo.R
import com.f1.demo.databinding.FragmentSplashBinding
import com.f1.demo.ui.base.BaseBindingFragment
import com.f1.demo.ui.splash.viewmodel.SplashViewModel

class SplashFragment: BaseBindingFragment<FragmentSplashBinding, SplashViewModel>() {

    override fun layout(): Int {
        return R.layout.fragment_splash;
    }

    override fun viewModelClass(): Class<SplashViewModel> {
        return SplashViewModel::class.java
    }
}