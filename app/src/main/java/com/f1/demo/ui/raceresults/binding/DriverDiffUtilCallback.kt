package com.f1.demo.ui.raceresults.binding

import androidx.recyclerview.widget.DiffUtil
import com.f1.demo.ui.raceresults.viewmodel.DriverResultViewModel

class DriverDiffUtilCallback( val oldList: List<DriverResultViewModel>,
                              val newList: List<DriverResultViewModel>): DiffUtil.Callback() {


    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].driverName.get() === newList[newItemPosition].driverName.get()
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].driverName.get() === newList[newItemPosition].driverName.get()
    }
}