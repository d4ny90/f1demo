package com.f1.demo.ui.raceresults.interactor

import com.f1.demo.data.sources.ergast.api.ErgastApi
import com.f1.demo.data.sources.ergast.model.RaceTable
import com.f1.demo.ui.raceresults.viewmodel.RaceTableViewModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RaceResultInteractor(val ergastApi: Single<ErgastApi>) {

    private val loadedRaceTables = ArrayList<RaceTableViewModel>()

    fun loadLatestRaceTable(): Single<List<RaceTableViewModel>> {
        return ergastApi.flatMap { it.getLastRaceResults() }
            .map { it.response.raceTable }
            .map {
                initRaceTablesViewModels(it)
                loadedRaceTables.find { race -> race.round == it.round }?.raceTable = it
                loadedRaceTables
            }
    }

    private fun initRaceTablesViewModels(raceTable: RaceTable) {
        (raceTable.round).downTo(1).forEach { roundToLoad ->
            loadedRaceTables.add(
                RaceTableViewModel(
                    dataLoadFunction = loadRaceTableForCurrentSeason(roundToLoad)
                ).apply {
                    onPageDisplayedCallback = this@RaceResultInteractor::onPageDisplayedCallback
                }
            )
        }
    }

    private fun onPageDisplayedCallback(round: Int) {
        loadedRaceTables.getOrNull(round - 1)?.preLoad()
        loadedRaceTables.getOrNull(round + 1)?.preLoad()
    }

    private fun loadRaceTableForCurrentSeason(round: Int): Single<RaceTable> {
        return ergastApi.flatMap { it.getRaceResults("current", "$round") }
            .map { it.response.raceTable }
    }

}