package com.f1.demo.ui.raceresults.view

import com.f1.demo.R
import com.f1.demo.databinding.FragmentSplashBinding
import com.f1.demo.ui.base.BaseBindingFragment
import com.f1.demo.ui.raceresults.viewmodel.RaceResultsViewModel

class RaceResultsFragment: BaseBindingFragment<FragmentSplashBinding, RaceResultsViewModel>() {

    override fun layout(): Int {
        return R.layout.fragment_race_result;
    }

    override fun viewModelClass(): Class<RaceResultsViewModel> {
        return RaceResultsViewModel::class.java
    }
}