package com.f1.demo.ui.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.f1.demo.BR
import com.f1.demo.di.ViewModelProviderFactory
import com.f1.demo.extensions.disposedBy
import dagger.android.support.AndroidSupportInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

abstract class BaseBindingFragment<B : ViewDataBinding, V : BaseViewModel>: Fragment() {

    var binding: B? = null

    var viewModel: V? = null
        internal set

    val disposables: CompositeDisposable = CompositeDisposable()
    val pauseDisposables: CompositeDisposable = CompositeDisposable()

    @Inject
    lateinit var viewModelFactory: ViewModelProviderFactory<V>

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)

        initViewModel()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        initBinding(inflater, container)

        return binding?.root
    }

    override fun onResume() {
        super.onResume()

        viewModel?.onResume()
        viewModel?.observeNavigationEvents()
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeBy {
                findNavController().navigate(it.navDirections)
            }
            ?.disposedBy(pauseDisposables)
    }

    override fun onPause() {
        super.onPause()

        pauseDisposables.clear()
        viewModel?.onPause()
    }

    private fun initBinding(inflater: LayoutInflater, container: ViewGroup?) {
        binding = DataBindingUtil.inflate(inflater, layout(), container, false)
        setBindingVariables(binding)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }

    private fun initViewModel() {

        viewModel = ViewModelProvider(this, viewModelFactory).get(viewModelClass())

    }

    protected open fun setBindingVariables(binding: B?) {
        binding?.setVariable(BR.viewModel, viewModel)
    }

    @LayoutRes
    protected abstract fun layout(): Int

    protected abstract fun viewModelClass(): Class<V>

}
