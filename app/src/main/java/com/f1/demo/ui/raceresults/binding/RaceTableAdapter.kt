package com.f1.demo.ui.raceresults.binding

import androidx.databinding.ViewDataBinding
import com.f1.demo.BR
import com.f1.demo.R
import com.f1.demo.base.adapter.BindingItemTypeAdapter
import com.f1.demo.ui.raceresults.viewmodel.RaceTableViewModel

class RaceTableAdapter: BindingItemTypeAdapter<ViewDataBinding, RaceTableViewModel> {

    override fun layout(): Int {
        return R.layout.item_race_table
    }

    override fun bind(binding: ViewDataBinding, item: RaceTableViewModel) {
        binding.setVariable(BR.viewModel, item)
        item.loadData()
    }

    override fun recycle(binding: ViewDataBinding, item: RaceTableViewModel) {
        super.recycle(binding, item)
        item.clearDisposables()
    }

    override fun attachToWindow(binding: ViewDataBinding, item: RaceTableViewModel) {
        super.attachToWindow(binding, item)
        item.onAttachToWindow()
    }

    override fun detachFromWindow(binding: ViewDataBinding, item: RaceTableViewModel) {
        super.detachFromWindow(binding, item)
        item.onDetachFromWindow()
    }

    override fun isItemHandled(item: RaceTableViewModel): Boolean {
        return true
    }

}