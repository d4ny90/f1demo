package com.f1.demo.ui.base

import androidx.lifecycle.ViewModel
import com.f1.demo.navigation.NavigationEvent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import io.reactivex.Observable

abstract class BaseViewModel : ViewModel() {


    private val navigationEvents = PublishSubject.create<NavigationEvent>()

    val disposables = CompositeDisposable()

    open fun onResume() {
        //implement in subclass to get callback
    }

    open fun onPause() {
        //implement in subclass to get callback
    }

    fun observeNavigationEvents(): Observable<NavigationEvent> {
        return navigationEvents
    }

    fun navigate(navigationEvent: NavigationEvent) {
        navigationEvents.onNext(navigationEvent)
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

}
