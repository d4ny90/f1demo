package com.f1.demo.ui.raceresults.viewmodel

import android.util.Log
import androidx.databinding.ObservableArrayList
import com.f1.demo.extensions.disposedBy
import com.f1.demo.ui.base.BaseViewModel
import com.f1.demo.ui.raceresults.interactor.RaceResultInteractor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class RaceResultsViewModel(interactor: RaceResultInteractor) : BaseViewModel() {

    val raceResults = ObservableArrayList<RaceTableViewModel>()

    init {
        interactor.loadLatestRaceTable()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onSuccess = {
                    raceResults.clear()
                    raceResults.addAll(it)
                },
                onError = {
                    Log.i("Error", it.stackTraceToString())
                }
            )

            .disposedBy(disposables)
    }
}