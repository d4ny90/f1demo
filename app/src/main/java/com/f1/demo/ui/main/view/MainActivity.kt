package com.f1.demo.ui.main.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.f1.demo.ui.main.viewmodel.MainViewModel
import com.f1.demo.R
import com.f1.demo.databinding.ActivityMainBinding
import com.f1.demo.di.ViewModelProviderFactory
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProviderFactory<MainViewModel>

    lateinit var binding: ActivityMainBinding
    lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        initViewModel()
        initBindings()
    }

    private fun initBindings() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.viewModel = viewModel
    }

    fun initViewModel() {
        viewModel = viewModelFactory.create(MainViewModel::class.java)
    }

}