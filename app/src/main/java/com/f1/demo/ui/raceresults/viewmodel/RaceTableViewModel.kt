package com.f1.demo.ui.raceresults.viewmodel

import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.f1.demo.data.sources.ergast.model.Driver
import com.f1.demo.data.sources.ergast.model.RaceTable
import com.f1.demo.extensions.disposedBy
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlin.reflect.KFunction1

class RaceTableViewModel(val dataLoadFunction: Single<RaceTable>, raceTable: RaceTable? = null) {

    private val disposables: CompositeDisposable = CompositeDisposable()

    var raceTable = raceTable
        set(value) {
            field = value
            if (value != null) {
                raceTrackDetails.set(buildRaceTrackDetailsViewModel(value))
                driverResults.clear()
                driverResults.addAll(buildDriverResults(value))
            }
            round = value?.round
            isLoaded.set(value != null)
        }

    var round: Int? = 0
        private set

    val isLoaded = ObservableBoolean(raceTable != null)

    val raceTrackDetails = ObservableField<RaceTrackDetailsViewModel>()

    val driverResults = ObservableArrayList<DriverResultViewModel>()

    var onPageDisplayedCallback: KFunction1<@ParameterName(name = "round") Int, Unit>? = null

    fun loadData() {
        if (!isLoaded.get()) {
            dataLoadFunction
                .map {
                    Log.i("DDDD -> ", "thread  " + Thread.currentThread().name)

                    it }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    Log.i("DDDD -> ", "Subscribing round " + round)
                }
                .subscribeBy {
                    raceTable = it
                }
                .disposedBy(disposables)
        }
    }

    fun onAttachToWindow() {
        round?.let { onPageDisplayedCallback?.invoke(it) }
    }

    fun onDetachFromWindow() {

    }

    private fun buildRaceTrackDetailsViewModel(raceTable: RaceTable): RaceTrackDetailsViewModel {
        return RaceTrackDetailsViewModel().apply {
            val race = raceTable.races.firstOrNull()
            raceName.set(race?.raceName.orEmpty())
            trackName.set(race?.circuit?.name.orEmpty())
            country.set(race?.circuit?.location?.country.orEmpty())
            seasonNumber.set(raceTable.season)
            roundNumber.set(raceTable.round)
        }
    }

    private fun buildDriverResults(raceTable: RaceTable): List<DriverResultViewModel> {
        val race = raceTable.races.firstOrNull() ?: return ArrayList()

        return race.results.map { result ->
            DriverResultViewModel().apply {
                driverName.set(buildDisplayDriverName(result.driver))
                driverNumber.set(result.number)
                constructorName.set(result.constructor.name)
                position.set(result.position)
                time.set(result.time?.time ?: result.status)
                points.set(result.points)
            }
        }
    }

    private fun buildDisplayDriverName(driver: Driver): String {
        return "${driver.givenName.first()}. ${driver.familyName}"
    }

    fun clearDisposables() {
        disposables.clear()
    }

    fun preLoad() {
        loadData()
    }
}