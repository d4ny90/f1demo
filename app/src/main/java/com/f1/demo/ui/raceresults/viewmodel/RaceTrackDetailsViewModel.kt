package com.f1.demo.ui.raceresults.viewmodel

import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt

class RaceTrackDetailsViewModel {

    val raceName = ObservableField<String>()

    val trackName = ObservableField<String>()

    val country = ObservableField<String>()

    val seasonNumber = ObservableInt()

    val roundNumber = ObservableInt()

}