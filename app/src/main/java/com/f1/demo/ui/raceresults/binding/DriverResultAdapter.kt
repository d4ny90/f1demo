package com.f1.demo.ui.raceresults.binding

import androidx.databinding.ViewDataBinding
import com.f1.demo.BR
import com.f1.demo.R
import com.f1.demo.base.adapter.BindingItemTypeAdapter
import com.f1.demo.ui.raceresults.viewmodel.DriverResultViewModel

class DriverResultAdapter: BindingItemTypeAdapter<ViewDataBinding, DriverResultViewModel> {

    override fun layout(): Int {
        return R.layout.item_driver_result
    }

    override fun bind(binding: ViewDataBinding, item: DriverResultViewModel) {
        binding.setVariable(BR.viewModel, item)
    }

    override fun recycle(binding: ViewDataBinding, item: DriverResultViewModel) {
        super.recycle(binding, item)
    }
    override fun isItemHandled(item: DriverResultViewModel): Boolean {
        return true
    }

}