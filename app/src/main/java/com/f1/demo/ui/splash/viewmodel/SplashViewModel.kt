package com.f1.demo.ui.splash.viewmodel

import com.f1.demo.extensions.disposedBy
import com.f1.demo.ui.base.BaseViewModel
import com.f1.demo.navigation.NavigationEvent
import com.f1.demo.ui.splash.view.SplashFragmentDirections
import io.reactivex.Single
import io.reactivex.rxkotlin.subscribeBy
import java.util.concurrent.TimeUnit

class SplashViewModel: BaseViewModel() {

    init {
        Single.just(1)
            .delay(3, TimeUnit.SECONDS)
            .subscribeBy {
                navigate(NavigationEvent(SplashFragmentDirections.openRaceResults()))
            }
            .disposedBy(disposables)
    }
}