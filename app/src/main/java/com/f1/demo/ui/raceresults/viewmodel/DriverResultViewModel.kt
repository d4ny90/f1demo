package com.f1.demo.ui.raceresults.viewmodel

import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt

class DriverResultViewModel {

    val position = ObservableInt()

    val driverName = ObservableField<String>()

    val driverNumber = ObservableField<String>()

    val constructorName = ObservableField<String>()

    val time = ObservableField<String>()

    val points = ObservableInt()

}