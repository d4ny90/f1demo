package com.f1.demo.ui.raceresults

import com.f1.demo.data.sources.ergast.api.ErgastApi
import com.f1.demo.di.ViewModelProviderFactory
import com.f1.demo.ui.raceresults.interactor.RaceResultInteractor
import com.f1.demo.ui.raceresults.viewmodel.RaceResultsViewModel
import dagger.Module
import dagger.Provides
import io.reactivex.Single

@Module
class RaceResultsModule {

    @Provides
    fun provideRaceResultsInteractor(ergastApi: Single<ErgastApi>): RaceResultInteractor {
        return RaceResultInteractor(ergastApi)
    }

    @Provides
    internal fun provideViewModel(interactor: RaceResultInteractor): ViewModelProviderFactory<RaceResultsViewModel> {

        return object : ViewModelProviderFactory<RaceResultsViewModel>() {
            override fun create(): RaceResultsViewModel {
                return RaceResultsViewModel(interactor)
            }
        }
    }
}