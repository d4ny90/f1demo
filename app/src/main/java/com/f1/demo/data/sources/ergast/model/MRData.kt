package com.f1.demo.data.sources.ergast.model

import com.google.gson.annotations.SerializedName

open class MRData {

    @SerializedName("limit")
    val limit: Int = 0

    @SerializedName("offset")
    val offset: Int = 0

    @SerializedName("total")
    val total: Int = 0
}