package com.f1.demo.data.sources.ergast.model

import com.google.gson.annotations.SerializedName

data class Time(
    @SerializedName("time")
    val time: String,
)