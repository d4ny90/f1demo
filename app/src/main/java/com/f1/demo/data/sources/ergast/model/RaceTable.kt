package com.f1.demo.data.sources.ergast.model

import com.google.gson.annotations.SerializedName

data class RaceTable(

    @SerializedName("season")
    val season: Int,

    @SerializedName("round")
    val round: Int,

    @SerializedName("Races")
    val races: List<Race>,


)