package com.f1.demo.data.network

import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Single

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

abstract class RestClientProvider<T>(
    private val okHttpClientBuilder: OkHttpClient.Builder,
    private val gson: Gson
) {

    protected abstract fun baseUrl(): Single<String>

    protected abstract fun clientClass(): Class<*>

    fun create(): Single<T> {
        return baseUrl()
            .map(this::createClient)
    }

    @Suppress("UNCHECKED_CAST")
    private fun createClient(baseUrl: String): T {
        val retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClientBuilder.build())
            .build()

        return retrofit.create(clientClass() as Class<Any>) as T
    }

    /**
     * Override this method if you would like to get notified before a request is sent
     */
    protected open fun onSendingRequest(request: Request): Request {
        return request
    }

    private val requestInterceptor = { chain: Interceptor.Chain ->

        val request = onSendingRequest(chain.request())

        chain.proceed(request)
    }

    init {
        okHttpClientBuilder.addInterceptor(requestInterceptor)
    }
}
