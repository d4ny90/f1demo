package com.f1.demo.data.sources.ergast.model

import com.google.gson.annotations.SerializedName

data class RaceResultMrDataResponse(
    @SerializedName("RaceTable")
    val raceTable: RaceTable
): MRData()