package com.f1.demo.data.sources.ergast.model

import com.google.gson.annotations.SerializedName

data class Race(

    @SerializedName("season")
    val season: String,

    @SerializedName("round")
    val round: String,

    @SerializedName("raceName")
    val raceName: String,

    @SerializedName("Circuit")
    val circuit: Circuit,

    @SerializedName("Results")
    val results: List<Result>,






)