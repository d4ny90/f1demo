package com.f1.demo.data.sources.ergast.model

import com.google.gson.annotations.SerializedName

data class QualifyingResponse(@SerializedName("MRData") val mrData10: Any)