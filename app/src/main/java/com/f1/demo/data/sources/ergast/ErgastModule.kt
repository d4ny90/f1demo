package com.f1.demo.data.sources.ergast

import com.f1.demo.data.sources.ergast.api.ErgastApi
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import io.reactivex.Single
import okhttp3.OkHttpClient
import javax.inject.Named

@Module
class ErgastModule {


    @Provides
    fun scoreClient(clientProvider: ErgastClientProvider): Single<ErgastApi> {
        return clientProvider.create()
    }

    @Provides
    fun scoreClientProvider(okHttpClient: OkHttpClient.Builder): ErgastClientProvider {
        return ErgastClientProvider(okHttpClient, Gson())
    }

}