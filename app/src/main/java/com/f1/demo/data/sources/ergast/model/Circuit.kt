package com.f1.demo.data.sources.ergast.model

import com.google.gson.annotations.SerializedName

data class Circuit(
    @SerializedName("circuitName")
    val name: String,

    @SerializedName("Location")
    val location: Location
)
