package com.f1.demo.data.sources.ergast.model

import com.google.gson.annotations.SerializedName

data class Constructor(
    @SerializedName("name")
    val name: String
)
