package com.f1.demo.data.sources.ergast.model

import com.google.gson.annotations.SerializedName

data class Result(


    @SerializedName("number")
    val number: String,

    @SerializedName("position")
    val position: Int,

    @SerializedName("positionText")
    val positionText: String,

    @SerializedName("points")
    val points: Int,

    @SerializedName("Driver")
    val driver: Driver,

    @SerializedName("Constructor")
    val constructor: Constructor,

    @SerializedName("grid")
    val grid: String,

    @SerializedName("laps")
    val laps: Int,

    @SerializedName("status")
    val status: String,

    @SerializedName("Time")
    val time: Time?,
)