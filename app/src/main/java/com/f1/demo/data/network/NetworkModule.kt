package com.f1.demo.data.network
import android.content.Context
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.io.File
import java.util.concurrent.TimeUnit

@Module
class NetworkModule {


    @Provides
    internal fun provideOkHttpClientBuilder(context: Context): OkHttpClient.Builder {

        val httpCacheDirectory = File(context.cacheDir, "httpCache")
        val cache = Cache(httpCacheDirectory, (10 * 1024 * 1024).toLong())

        val okHttpClientBuilder = OkHttpClient().newBuilder()
            .cache(cache)
            .readTimeout(20, TimeUnit.SECONDS)
            .connectTimeout(20, TimeUnit.SECONDS)

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        okHttpClientBuilder.addInterceptor(logging)
        return okHttpClientBuilder
    }
}