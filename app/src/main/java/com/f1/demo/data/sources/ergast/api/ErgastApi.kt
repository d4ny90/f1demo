package com.f1.demo.data.sources.ergast.api

import com.f1.demo.data.sources.ergast.model.RaceResultsResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface ErgastApi {

    @GET("{year}/{round}/results")
    fun getRaceResults(@Path("year") year: String?, @Path("round") round: String?): Single<RaceResultsResponse>

    @GET("current/last/results")
    fun getLastRaceResults(): Single<RaceResultsResponse>
}