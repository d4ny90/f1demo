package com.f1.demo.data.sources.ergast

import com.f1.demo.data.network.RestClientProvider
import com.f1.demo.data.sources.ergast.api.ErgastApi
import com.google.gson.Gson
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.Request

class ErgastClientProvider(okHttpClient: OkHttpClient.Builder, gson: Gson) :
    RestClientProvider<ErgastApi>(okHttpClient, gson) {


    override fun baseUrl(): Single<String> {
        return Single.just("http://ergast.com/api/f1/")
    }

    override fun clientClass(): Class<*> {
        return ErgastApi::class.java
    }

    override fun onSendingRequest(request: Request): Request {
        return request.newBuilder().url("${request.url()}.json").build()
    }
}