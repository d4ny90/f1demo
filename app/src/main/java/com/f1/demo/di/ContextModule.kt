package com.f1.demo.di

import android.content.Context
import com.f1.demo.MainApplication
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class ContextModule {

    @Singleton
    @Binds
    abstract fun context(appInstance: MainApplication): Context

}