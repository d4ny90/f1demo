package com.f1.demo.di

import com.f1.demo.ui.main.view.MainActivity
import com.f1.demo.ui.main.MainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(includes = [FragmentsModule::class])
abstract class ActivitiesModule {

    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    internal abstract fun bindMainActivity(): MainActivity

}
