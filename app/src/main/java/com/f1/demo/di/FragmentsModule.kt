package com.f1.demo.di

import com.f1.demo.ui.raceresults.RaceResultsModule
import com.f1.demo.ui.raceresults.view.RaceResultsFragment
import com.f1.demo.ui.splash.SplashModule
import com.f1.demo.ui.splash.view.SplashFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentsModule {

    @ContributesAndroidInjector(modules = [SplashModule::class])
    internal abstract fun bindSplashFragment(): SplashFragment

    @ContributesAndroidInjector(modules = [RaceResultsModule::class])
    internal abstract fun bindRaceResultsFragment(): RaceResultsFragment

}