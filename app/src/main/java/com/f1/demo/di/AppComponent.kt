package com.f1.demo.di

import com.f1.demo.MainApplication
import com.f1.demo.data.network.NetworkModule
import com.f1.demo.data.sources.ergast.ErgastModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ActivitiesModule::class,
        ErgastModule::class,
        NetworkModule::class,
    ContextModule::class
    ]
)
interface AppComponent: AndroidInjector<MainApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: MainApplication): Builder

        fun build(): AppComponent
    }

    override fun inject(application: MainApplication)

}
