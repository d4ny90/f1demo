package com.f1.demo.base.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.f1.demo.R

class BindingListAdapter<T> : androidx.recyclerview.widget.RecyclerView.Adapter<BindingViewHolder<ViewDataBinding, T>>() {

    private val typeAdapters = ArrayList<BindingItemTypeAdapter<ViewDataBinding, T>>()

    var list = ArrayList<T>()
        set(value) {
            this.list.clear()
            this.list.addAll(value)
        }

    fun addTypeAdapter(adapter: BindingItemTypeAdapter<ViewDataBinding, T>) {
        this.typeAdapters.add(adapter)
    }

    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
    ): BindingViewHolder<ViewDataBinding, T> {

        val inflater = LayoutInflater.from(parent.context)

        val layout: Int = if (viewType > -1) {
            val typeAdapter = typeAdapters[viewType]
            typeAdapter.layout()
        } else {
            R.layout.list_item_type_adapter_missing_error
        }

        val dataBinding = DataBindingUtil.inflate<ViewDataBinding>(inflater, layout, parent, false)

        return BindingViewHolder(dataBinding)
    }

    override fun onBindViewHolder(holder: BindingViewHolder<ViewDataBinding, T>, position: Int) {

        val viewType = getItemViewType(position)
        val item = getItem(position)

        if (viewType > -1) {
            val typeAdapter = typeAdapters[viewType]
            typeAdapter.bind(holder.binding, item)
        }

        holder.item = item

        holder.binding.executePendingBindings()

    }

    override fun onViewDetachedFromWindow(holder: BindingViewHolder<ViewDataBinding, T>) {
        super.onViewDetachedFromWindow(holder)

        typeAdapters.forEach {
            val item: T? = holder.item

            if (item != null && it.isItemHandled(item)) {
                it.detachFromWindow(holder.binding, item)
                return@forEach
            }
        }
    }

    override fun onViewAttachedToWindow(holder: BindingViewHolder<ViewDataBinding, T>) {
        super.onViewAttachedToWindow(holder)
        typeAdapters.forEach {
            val item: T? = holder.item

            if (item != null && it.isItemHandled(item)){
                it.attachToWindow(holder.binding, item)
                return@forEach
            }
        }
    }

    override fun onViewRecycled(holder: BindingViewHolder<ViewDataBinding, T>) {
        super.onViewRecycled(holder)

        typeAdapters.forEach {
            val item: T? = holder.item

            if (item != null && it.isItemHandled(item)){
                it.recycle(holder.binding, item)
                return@forEach
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        val item = getItem(position)
        for (i in typeAdapters.indices) {
            if (typeAdapters[i].isItemHandled(item)) {
                return i
            }
        }
        return -1
    }

    private fun getItem(position: Int): T {
        return list[position]
    }

    override fun getItemCount(): Int {
        return list.size
    }

}
