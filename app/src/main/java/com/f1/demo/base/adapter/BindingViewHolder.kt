package com.f1.demo.base.adapter

import androidx.databinding.ViewDataBinding

class BindingViewHolder<B : ViewDataBinding, T> internal constructor(val binding: B) :
    androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root) {

    var item: T? = null
}
