package com.f1.demo.base.adapter

import androidx.databinding.ViewDataBinding
import androidx.annotation.LayoutRes

interface BindingItemTypeAdapter<B : ViewDataBinding, T> {

    /**
     * This method determines whether the current item is handled by this type adapter
     *
     * @param item data for row item
     * @param position position of row item
     * @return true if the passed item is handled by this type adapter; otherwise false
     */
    fun isItemHandled(item: T): Boolean = true

    /**
     * Bind variables to xml
     *
     * @param binding      current item binding object
     * @param item         item to bind to xml
     */
    fun bind(binding: B, item: T) {}

    /**
     * Called when item is recycled
     */
    fun recycle(binding: B, item: T) {}

    /**
     * Layout id to use for the current item type
     *
     * @return layout id e.g.: R.layout.list_item_van or R.layout.list_item_car
     */
    @LayoutRes
    fun layout(): Int

    /**
     * Called when item goes off screen
     */
    fun detachFromWindow(binding: B, item: T) {}

    fun attachToWindow(binding: B, item: T) {}
}
