package com.f1.demo

import android.view.View
import androidx.databinding.BindingAdapter
import com.airbnb.lottie.LottieAnimationView

@BindingAdapter("invisibility")
fun bindInvisibility(view: View, isVisible: Boolean) {
    if (isVisible) {
        view.visibility = View.VISIBLE
    } else {
        view.visibility = View.INVISIBLE
    }
}

@BindingAdapter("visibility")
fun bindVisibility(view: View, isVisible: Boolean) {
    if (isVisible) {
        view.visibility = View.VISIBLE
    } else {
        view.visibility = View.GONE
    }
}


@BindingAdapter("playLottieAnimation")
fun bindPlayLottieAnimation(view: LottieAnimationView, play: Boolean) {
    if (play) {
        view.playAnimation()
    }
}
